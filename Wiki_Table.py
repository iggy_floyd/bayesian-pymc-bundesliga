'''
    Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
    
    A tool to scrape data from wiki-pages
'''
from bs4 import BeautifulSoup
import urllib2
import warnings
from  StringIO import StringIO
import pandas as pd
warnings.filterwarnings('ignore')


class MyWikiParser(BeautifulSoup):
    ''' BeautifulSoup parser: parses <table> </table> tags'''
    
    def __init__(self,page,table_class_properties,table_header):
        BeautifulSoup.__init__(self,page,fromEncoding="UTF-8")
        self.recording = 0
        self.data = []
        self.table_class_properties = table_class_properties
        self.table_header = table_header

    def handle_data(self,column_tag="td"):
        tables = self.findAll("table", self.table_class_properties)         
        for table in tables:    
            first_row=table.find("tr")    
            first_cell_in_first_row = first_row.find(column_tag)
            rows = []
            if (first_cell_in_first_row is not None) and (self.table_header in str(first_cell_in_first_row)):        
                for row in table.findAll("tr"):            
                    vals=row.find_all('td')
                    vals=map(lambda x: x.text.encode('utf-8').split('!')[1] if len( x.text.encode('utf-8').split('!'))>1 else x.text.encode('utf-8').split('!')[0], vals) # fix some text data            
#                    rows.append([val.text.encode('utf-8').replace('\xe2\x95\xb2','/').replace('\xe2\x80\x93','-') for val in vals])
                    rows.append([val.replace('\xe2\x95\xb2','/').replace('\xe2\x80\x93','-') for val in vals])
                self.data=rows

        
class Wiki_Table(object):
    ''' exctracts values for the table from Wiki-Page files '''
 
    def __init__(self,url,table_class_properties,table_header,column_tag="td"):
        self.table = []
        header = {'User-Agent': 'Mozilla/5.0'} #Needed to prevent 403 error on Wikipedia        
        req = urllib2.Request(url,headers=header)
        page = urllib2.urlopen(req)       
        parser=MyWikiParser(page,table_class_properties,table_header)
        parser.handle_data(column_tag)   
        self.table = parser.data
        


# a page containing the data
wikipage='https://en.wikipedia.org/wiki/2013%E2%80%9314_Bundesliga#Results'

# a pattern on the header to find the table among others
title_pattern='Home ╲ Away'

# a pattern on the class properties of the table used to find it 
table_props={"class" : "wikitable"}

# parse the table from the wiki page
wt=Wiki_Table(wikipage,table_props,title_pattern)

# create a dataframe on the data
table_csv=map(lambda x: ','.join(x),wt.table)
table_csv='\n'.join(table_csv)
data = StringIO(table_csv)

#df = pd.read_csv(data,delimiter=',')
df = pd.read_csv(data,delimiter=',',index_col=0)

print df
